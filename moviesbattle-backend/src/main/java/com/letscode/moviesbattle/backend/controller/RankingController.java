package com.letscode.moviesbattle.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.letscode.moviesbattle.backend.dto.RankingDTO;
import com.letscode.moviesbattle.backend.service.RankingService;

/**
 * API para consulta o ranking de jogadores
 * 
 * @author Vitor.Campos
 * @since 24 jan 2022
 */
@RestController
@RequestMapping("/api/ranking")
public class RankingController {
	
	@Autowired
	private RankingService service;
	
	
	@GetMapping
	public List<RankingDTO> consultarRanking() {
		List<RankingDTO> listaRanking = service.pesquisarRanking();
		return listaRanking;
	}
	
	

}
