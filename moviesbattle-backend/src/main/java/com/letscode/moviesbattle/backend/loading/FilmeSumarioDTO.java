package com.letscode.moviesbattle.backend.loading;

import com.fasterxml.jackson.annotation.JsonAlias;

public class FilmeSumarioDTO {

	@JsonAlias("Title")
	private String title;
	
	@JsonAlias("Year")
	private String year;
	
	@JsonAlias("imdbID")
	private String imdgID;
	
	@JsonAlias("Type")
	private String type;
	
	@JsonAlias("Poster")
	private String poster;

	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getImdgID() {
		return imdgID;
	}

	public void setImdgID(String imdgID) {
		this.imdgID = imdgID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}
	
}
