package com.letscode.moviesbattle.backend.loading;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.letscode.moviesbattle.backend.entity.Filme;

public class FilmeDetalheDTO {

	@JsonAlias("Title")
	private String title;
	
	@JsonAlias("Year")
	private String year;
	
	@JsonAlias("Director")
	private String director;
	
	@JsonAlias("Actors")
	private String actors;
	
	@JsonAlias("Country")
	private String country;
	
	@JsonAlias("Poster")
	private String poster;
	
	
	private String imdbRating; //"imdbRating": "7.4",
	
	private String imdbVotes; //"imdbVotes": "473,820",
	
	private String imdbID; //imdbID

	private BigDecimal pontuacaoFilme;
	
	
	public FilmeDetalheDTO() {
	}
	
	
	public FilmeDetalheDTO(Filme f) {
		this.title = f.getTitle();
		this.year = f.getYear();
		this.director = f.getDirector();
		this.actors = f.getActors();
		this.country = f.getCountry();
		this.poster = f.getPoster();
		this.imdbRating = f.getImdbRating();
		this.imdbVotes = f.getImdbVotes();
		this.imdbID = f.getImdbID();
		this.pontuacaoFilme = f.getPontuacaoFilme();
	}



	public Filme toFilme() {
		Filme f = new Filme();
		f.setTitle(title);
		f.setYear(year);
		f.setDirector(director);
		f.setActors(actors);
		f.setCountry(country);
		f.setPoster(poster);
		f.setImdbRating(imdbRating);
		f.setImdbVotes(imdbVotes);
		f.setImdbID(imdbID);
		return f;
	}
	
	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getImdbVotes() {
		return imdbVotes;
	}

	public void setImdbVotes(String imdbVotes) {
		this.imdbVotes = imdbVotes;
	}

	public String getImdbID() {
		return imdbID;
	}

	public void setImdbID(String imdbID) {
		this.imdbID = imdbID;
	}


	public BigDecimal getPontuacaoFilme() {
		return pontuacaoFilme;
	}


	public void setPontuacaoFilme(BigDecimal pontuacaoFilme) {
		this.pontuacaoFilme = pontuacaoFilme;
	}
	
	
}
