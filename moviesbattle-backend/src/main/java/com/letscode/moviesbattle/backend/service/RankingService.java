package com.letscode.moviesbattle.backend.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.letscode.moviesbattle.backend.dto.RankingDTO;
import com.letscode.moviesbattle.backend.repository.RankingRepository;

/**
 * Serviço para ranking
 * 
 * @author Vitor.Campos
 * @since 24 jan 2022
 */
@Service
public class RankingService {

	@Autowired
	private RankingRepository repo;
	
	/**
	 * Pesquisa pela lista de ranking, ordena e indexa
	 * @return
	 */
	public List<RankingDTO> pesquisarRanking() {
		//1.pesquisa
		List<RankingDTO> listaRanking = repo.pesquisarRankingDTO();
		
		//2.ordena da maior pontuacao para a menor
		List<RankingDTO> listaRankingOrdenada = listaRanking.stream()
			.sorted(Comparator.reverseOrder() )
			.collect( Collectors.toList() );

		//3.atribui colocao
		int i = 1;
		for (RankingDTO dto : listaRankingOrdenada) {
			dto.setColocacao( i++ );
		}
		
		return listaRankingOrdenada;
	}

}
