package com.letscode.moviesbattle.backend.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
@Configuration
public class MyWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private MyUserDetailsService myUserDetailsService;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
        	.authorizeHttpRequests()
        		.antMatchers("/auth").permitAll()// Apenas foi configurado
        		.antMatchers("/h2-console/**").permitAll()
        		.antMatchers("/api/**").permitAll()
        		.anyRequest().permitAll()
            .and()
            .formLogin().disable()
            .httpBasic()
            ;
    }	
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService)
        	.passwordEncoder(new BCryptPasswordEncoder() );
    }    

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable()
//        	.httpBasic()
//        	.and()
//        	.authorizeHttpRequests()
//            .antMatchers("/api/**").authenticated()
//            .and()
//            .formLogin().disable();
//    }	
    
    
}
