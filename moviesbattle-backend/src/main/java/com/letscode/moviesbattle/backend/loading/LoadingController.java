package com.letscode.moviesbattle.backend.loading;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.letscode.moviesbattle.backend.entity.Filme;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;

@RestController
@RequestMapping("/api/data")
public class LoadingController {
	
	
	@Autowired
	private FilmeRepository filmeRepository;
	

	@GetMapping("/full")
	public String dataFull() {
		String msgLoad = dataLoad();
		String msgFormat = dataFormat();
		return msgLoad + '\n' + msgFormat;
	}
	
	
	/**
	 * Carrega filmes aleatórios usando palavras chaves relevantes
	 * @return
	 */
//	@GetMapping("/load")
	public String dataLoad() {
		//0. verifica se base está vazia
		if (filmeRepository.count() > 0) {
			throw new RuntimeException("Base de filmes já está carregada");
		}
		
		
		List<FilmeSumarioDTO> listaFilmeSumario = new ArrayList<>();
		
		RestTemplate template = new RestTemplate();
		
		String listaChaveDeBusca[] = {"future"
				,"knight"
				,"mission"
				,"wars"
				,"warrior"
				,"rocky"
				,"good"
				,"2001"
				,"matrix"
				,"rings"
				,"mens"
				,"avengers"
				,"indiana"
				,"alien"
				,"casablanca"
				,"wind"
				,"black"
				,"titanic"
				,"godfather"
		};
				
			
		//1. busca o sumario dos filmes:
		final String URL_SEARCH_TEMPLATE = "https://www.omdbapi.com/?apikey=2f409c44&type=movie&page=1&s=%s";
				
		for (String chaveBusca : listaChaveDeBusca) {
			String url = String.format(URL_SEARCH_TEMPLATE, chaveBusca);
		
			try {
				SearchResultDTO searchResult = template.getForObject(url, SearchResultDTO.class);
	
				listaFilmeSumario.addAll( searchResult.toListFilmeSumario() );//aggrepa filmes
				
			} catch (RestClientException e) {
				//ignora e continua
				System.out.println("Erro ao buscar sumários de filme: " + e.getMessage() );
			}
		}
		
		
		//2. busca o detalhe de cada filme
		List<FilmeDetalheDTO> listaFilmeDetalhe = new ArrayList<>();
		
		final String URL_DETAIL_TEMPLATE = "https://www.omdbapi.com/?apikey=2f409c44&type=movie&i=%s";
		
		for (FilmeSumarioDTO filmeSumario : listaFilmeSumario) {
			String url = String.format(URL_DETAIL_TEMPLATE, filmeSumario.getImdgID());
			
			
			try {
				FilmeDetalheDTO filmeDetalhe = template.getForObject(url, FilmeDetalheDTO.class);
				
				listaFilmeDetalhe.add( filmeDetalhe );
			} catch (RestClientException e) {
				// ignora e continua
				System.out.println("Erro ao buscar detalhe do filme: " + e.getMessage() );
			}
		}
		
		//3. coleta lista de filmes
		List<Filme> listaFilme = listaFilmeDetalhe.stream()
			.map(d -> d.toFilme() )
			.collect( Collectors.toList() );
		

		//4. finalmente salva na base
		filmeRepository.saveAll( listaFilme );
		
		return "Filmes carregados " + listaFilme.size();
	}
	
	
//	@GetMapping("/format")
	public String dataFormat() {
		List<Filme> listaFilme = filmeRepository.findAll();
		
		//4. atualizar a pontuação de cada filme
		for (Filme filme : listaFilme) {
			try {
				filme.atualizarPontuacao();
				filmeRepository.save( filme );
				
			}catch(Exception e) {
				System.out.println("Erro ao atualizar pontuacao de filme: " + e.getMessage() );
				System.out.println("     rating: " + filme.getImdbRating() );
				System.out.println("     votes : " + filme.getImdbVotes() );
			}
		}

		return "Pontuação de filme atualizada com sucesso";
	}

	
}
