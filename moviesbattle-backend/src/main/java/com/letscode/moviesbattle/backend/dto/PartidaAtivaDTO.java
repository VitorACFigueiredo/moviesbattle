package com.letscode.moviesbattle.backend.dto;

import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.entity.Pergunta;

public class PartidaAtivaDTO {

	private String jogadorNome;
	

	private PerguntaDTO perguntaRecemRespondida;
	
	private PerguntaDTO perguntaAtual;

	
	public PartidaAtivaDTO() {}
	
	public PartidaAtivaDTO(Partida p) {
		this.jogadorNome = p.getJogador().getNome();
	}


	public PartidaAtivaDTO(Partida partida, Pergunta proximaPergunta) {
		this(partida);
		this.perguntaAtual = new PerguntaDTO( proximaPergunta );
	}

	
	public PartidaAtivaDTO(Partida partida, Pergunta perguntaRecemRespondida, Pergunta proximaPergunta) {
		this(partida);
		this.perguntaRecemRespondida = new PerguntaDTO( perguntaRecemRespondida );
		this.perguntaAtual = new PerguntaDTO( proximaPergunta );
	}

	
	

	//acessores...
	
	public String getJogadorNome() {
		return jogadorNome;
	}
	
	public PerguntaDTO getPerguntaRecemRespondida() {
		return perguntaRecemRespondida;
	}

	public void setPerguntaRecemRespondida(PerguntaDTO perguntaRecemRespondida) {
		this.perguntaRecemRespondida = perguntaRecemRespondida;
	}

	public PerguntaDTO getPerguntaAtual() {
		return perguntaAtual;
	}

	public void setPerguntaAtual(PerguntaDTO perguntaAtual) {
		this.perguntaAtual = perguntaAtual;
	}

	public void setJogadorNome(String jogadorNome) {
		this.jogadorNome = jogadorNome;
	}
	
}
