package com.letscode.moviesbattle.backend.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.letscode.moviesbattle.backend.repository.UsuarioRepository;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UsuarioRepository repository;
	
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Optional<Usuario> opUsuario = repository.findByLogin(login);
		
		if (opUsuario.isEmpty()) {
			throw new UsernameNotFoundException("Usuário não encontrado");
		}

		return opUsuario.get();
	}

}
