package com.letscode.moviesbattle.backend.dto;

import com.letscode.moviesbattle.backend.entity.Jogador;

public class JogadorDTO {
	
	private final Long id;
	
	private final String nome;

	
	public JogadorDTO(Jogador jogador) {
		this.id = jogador.getId();
		this.nome = jogador.getNome();
	}


	public Long getId() {
		return id;
	}


	public String getNome() {
		return nome;
	}


	public Jogador toJogador() {
		return new Jogador(id, nome);
	}

	
	

}
