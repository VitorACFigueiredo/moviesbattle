package com.letscode.moviesbattle.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.letscode.moviesbattle.backend.entity.ParDeFilme;
import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.entity.Pergunta;
import com.letscode.moviesbattle.backend.repository.PerguntaRepository;

/**
 * Serviços para pergunta
 * 
 * @author Vitor.Campos
 * @since 22 jan 2021
 */
@Service
public class PerguntaService {

	@Autowired
	private PerguntaRepository repository;
	
	
	@Autowired
	private FilmeService filmeService;
	
	@Autowired
	private PartidaService partidaService;
	
	/**
	 * Cria um pergunta para a partida
	 * @param partida
	 * @return
	 */
	public Pergunta criarPergunta(Partida partida) {
		ParDeFilme parDeFilme = filmeService.criarParDeFilmeUnico();
		
		Pergunta novaPergunta = new Pergunta(partida, parDeFilme);
		
		novaPergunta = repository.save(novaPergunta);
		return novaPergunta;
	}
	

	/**
	 * Recarrega uma partida
	 * @param perguntaId
	 * @return
	 */
	private Pergunta recarregarPerguntaPeloId(Long perguntaId) {
		return repository.findById(perguntaId).get();
	}
	
	
	/**
	 * Verifica se pergunta foi respondida corretamente de acordo
	 * com a opção respondida
	 * @param perguntaId
	 * @param opcaoRespondida
	 * @return
	 */
	public boolean foiRespondidaCorretamente(Long perguntaId, Integer opcaoRespondida) {
		Pergunta p = recarregarPerguntaPeloId(perguntaId);
		return p.foiRespondidaCorretamente(opcaoRespondida);
	}

	
	/**
	 * Salva uma pergunta como correta
	 * @param perguntaId
	 * @return
	 */
	public Pergunta salvarPerguntaComoCorreta(Long perguntaId) {
		Pergunta perguntaEncontrada = repository.findById(perguntaId).get();
		perguntaEncontrada.registrarAcerto();
		
		return repository.save( perguntaEncontrada );
	}
	
	
	/**
	 * Salva uma pergunta como errada incrementano o contador de erros
	 * da partida ativo
	 * @param perguntaId
	 * @return
	 */
	public Pergunta salvarPerguntaComoErrada(Long perguntaId) {
		Pergunta perguntaErrada = repository.findById(perguntaId).get();
		perguntaErrada.registarComoIncorreto();
		
		Partida partidaAtual = perguntaErrada.getPartida();
		partidaService.incrementarContadorDeErros(partidaAtual);
		
		return repository.save( perguntaErrada );
	}



}
