package com.letscode.moviesbattle.backend.security;

public class UsuarioDTO {
	
	private Long jogadorId;
	
	private String jogadorNome;
	
	private String login;

	public UsuarioDTO(Usuario u) {
		this.login = u.getLogin();
		this.jogadorId = u.getJogador().getId();
		this.jogadorNome = u.getJogador().getNome();
	}


	public String getLogin() {
		return login;
	}
	public Long getJogadorId() {
		return jogadorId;
	}
	public String getJogadorNome() {
		return jogadorNome;
	}
	
	
}
