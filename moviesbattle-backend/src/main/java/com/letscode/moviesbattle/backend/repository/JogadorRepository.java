package com.letscode.moviesbattle.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.letscode.moviesbattle.backend.entity.Jogador;

@Repository
public interface JogadorRepository extends JpaRepository<Jogador, Long> {

}
