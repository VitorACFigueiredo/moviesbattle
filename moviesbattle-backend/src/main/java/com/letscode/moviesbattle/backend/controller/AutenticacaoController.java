package com.letscode.moviesbattle.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.letscode.moviesbattle.backend.security.LoginForm;
import com.letscode.moviesbattle.backend.security.MyUserDetailsService;
import com.letscode.moviesbattle.backend.security.Usuario;
import com.letscode.moviesbattle.backend.security.UsuarioDTO;

/**
 * API para realizar a autenticação de usuários
 * 
 * @author Vitor.Campos
 * @since 23 jan 2022
 */
@RestController
@RequestMapping("/auth")
public class AutenticacaoController {
	
	@Autowired
	private MyUserDetailsService myUserDetailsService;
	
	@PostMapping
	public ResponseEntity<UsuarioDTO> autenticar(@RequestBody LoginForm loginForm) {
		UserDetails userDetails = myUserDetailsService.loadUserByUsername( loginForm.getLogin() );
		
		UsuarioDTO usuarioDTO = new UsuarioDTO( (Usuario)userDetails );
		
		return ResponseEntity.ok( usuarioDTO );
	}
	

}
