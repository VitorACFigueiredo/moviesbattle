package com.letscode.moviesbattle.backend.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.letscode.moviesbattle.backend.dto.RankingDTO;
import com.letscode.moviesbattle.backend.entity.Jogador;
import com.letscode.moviesbattle.backend.entity.Partida;

@Repository
public class RankingRepository {
	
	
	@Autowired
	private EntityManager em;
	

	public List<RankingDTO> pesquisarRankingDTO() {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<RankingDTO> criteria = builder.createQuery(RankingDTO.class);
		Root<Partida> root = criteria.from(Partida.class);
		
		Join<Partida, Jogador> joinJogador = root.join("jogador");
		
		Path<Long> pathJogadorId = joinJogador.get("id");
		Path<String> pathJogadorNome = joinJogador.get("nome");

		Expression<Long> pathCountPartida = builder.count( root );
		Path<Long> pathQtdRespostasCorretas = root.get("quantidadeRespostasCorretas");
		Path<Long> pathQtdPerguntas = root.get("quantidadePerguntas");
		
		
		criteria.select( builder.construct(RankingDTO.class
						, pathJogadorId
						, pathJogadorNome
						, pathCountPartida
						, builder.sum( pathQtdRespostasCorretas )
						, builder.sum( pathQtdPerguntas )
						));
		
		criteria.groupBy( pathJogadorId, pathJogadorNome );
		
		List<RankingDTO> listaRanking = em.createQuery(criteria).getResultList();
		return listaRanking;
	}

	
//	public Long contarPartidasPorJogadorId(Long jogadorId) {
//		String query = " select count(*) from Partida p where p.jogador.id = :pJogadorId ";
//		Long qtdPartidas = em.createQuery( query, Long.class )
//				.setParameter("pJogadorId", jogadorId)
//				.getSingleResult();
//		return qtdPartidas;
//	}

	
}
