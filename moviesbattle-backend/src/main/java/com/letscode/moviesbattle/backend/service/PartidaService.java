package com.letscode.moviesbattle.backend.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.letscode.moviesbattle.backend.entity.Jogador;
import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.repository.JogadorRepository;
import com.letscode.moviesbattle.backend.repository.PartidaRepository;

/**
 * Serviços para partidas
 * 
 * @author Vitor.Campos
 * @since 22 jan 2022
 */
@Service
public class PartidaService {
	
	private static final String MSG_PARTIDA_ID_INEXISTENTE = "Identificador de partida inexistente: %s";
	
	@Autowired
	private PartidaRepository repository;
	
	@Autowired
	private JogadorRepository jogadorRepository;
	
	/**
	 * Cria um partida para o jogador
	 * @param jogadorId
	 * @return
	 */
	public Partida criarPartida(Long jogadorId) {
		Optional<Jogador> opJogador = jogadorRepository.findById(jogadorId);
		if (opJogador.isEmpty()) {
			throw new RuntimeException("ID de jogador inexistente: "+jogadorId);
		}
		
		Partida partida = new Partida();
		partida.setJogador( opJogador.get() );
		partida = repository.save(partida);
		return partida;
	}

	
	/**
	 * Encerrar uma partida totalizando os dados
	 * @param partidaId
	 * @return
	 */
	public Partida encerrarPartida(Long partidaId) {
		Partida partida = recarregarPartidaPeloId(partidaId);
		partida.totalizarEEncerrarPartida();
		return repository.save( partida );
	}


	/**
	 * Verifica se pode encerrar uma partida conforme
	 * o numero de erros foi atingido
	 * @param partidaId
	 * @return
	 */
	public Partida encerrarPartidaSemAtingirLimiteDeErros( Long partidaId ) { 		
		Partida partidaAtual = recarregarPartidaPeloId( partidaId );
		
		if (partidaAtual.jaAtingiuLimitePerguntasErradas() ) {
			partidaAtual = encerrarPartida( partidaId );
			partidaAtual.setFlagAtingiuLimiteErro(true);
		}
		
		return partidaAtual;
	}

	
	/**
	 * Recarrega um partida
	 * @param partidaId
	 * @return
	 */
	public Partida recarregarPartidaPeloId(Long partidaId) {
		Optional<Partida> opPartida = repository.findById(partidaId);
		if (opPartida.isPresent()) {
			Partida partida = opPartida.get();
			return partida;
		}
		
		throw new RuntimeException(String.format(MSG_PARTIDA_ID_INEXISTENTE, partidaId ) );
	}

	/**
	 * Incrementa o contato de erros da partida
	 * @param partida
	 * @return
	 */
	public Partida incrementarContadorDeErros(Partida partida) {
		partida.incrementarContadorPerguntasErradas();
		return repository.save(partida);
	}
	
	

}
