package com.letscode.moviesbattle.backend.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Partida implements Serializable {

	private static final int LIMITE_PERGUNTAS_ERRADAS = 3;

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	private Jogador jogador;
	
	@OneToMany(mappedBy = "partida")
	private List<Pergunta> listaPergunta;
	
	
	private Integer contadorPerguntasErradas = 0;
	
	
	//informações para calcular o ranking

	private Integer quantidadeRespostasCorretas = 1;

	private Integer quantidadePerguntas = -1;
	

	
	private Boolean flagAtingiuLimiteErro = false;
	
	private Boolean flagEncerrada = false;


	/**
	 * 
	 */
	private static final long serialVersionUID = 2184869114008108077L;

	public Long getId() {
		return id;
	}

	public Boolean getFlagEncerrada() {
		return flagEncerrada;
	}

	public Boolean getFlagAtingiuLimiteErro() {
		return flagAtingiuLimiteErro;
	}

	public void setFlagAtingiuLimiteErro(Boolean flagAtingiuLimiteErro) {
		this.flagAtingiuLimiteErro = flagAtingiuLimiteErro;
	}

	public Integer getQuantidadeRespostasCorretas() {
		return quantidadeRespostasCorretas;
	}

	public void setQuantidadeRespostasCorretas(Integer quantidadeRespostasCorretas) {
		this.quantidadeRespostasCorretas = quantidadeRespostasCorretas;
	}

	public List<Pergunta> getListaPergunta() {
		return listaPergunta;
	}

	public void setListaPergunta(List<Pergunta> listaPergunta) {
		this.listaPergunta = listaPergunta;
	}

	public Integer getContadorPerguntasErradas() {
		return contadorPerguntasErradas;
	}

	public void setContadorPerguntasErradas(Integer contadorPerguntasErradas) {
		this.contadorPerguntasErradas = contadorPerguntasErradas;
	}


	public void setFlagEncerrada(Boolean flagEncerrada) {
		this.flagEncerrada = flagEncerrada;
	}

	public Integer getQuantidadePerguntas() {
		return quantidadePerguntas;
	}

	public void setQuantidadePerguntas(Integer quantidadePerguntas) {
		this.quantidadePerguntas = quantidadePerguntas;
	}


	public Jogador getJogador() {
		return jogador;
	}

	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Partida other = (Partida) obj;
		return Objects.equals(id, other.id);
	}

	
	public void incrementarContadorPerguntasErradas() {
		this.contadorPerguntasErradas++;
	}

	
	public boolean jaAtingiuLimitePerguntasErradas() {
		return this.contadorPerguntasErradas >= LIMITE_PERGUNTAS_ERRADAS;
	}
	
	
	public void totalizarEEncerrarPartida() {
		//todas as perguntas (certas e erradas)
		this.quantidadePerguntas = this.listaPergunta.size();
		
		//perguntas certas
		this.quantidadeRespostasCorretas = this.listaPergunta.stream()
				.filter(p -> p.getPontuacaoPergunta() > 0)
				.collect( Collectors.toList() )
				.size();
		
		//seta flag
		this.flagEncerrada = true;
	}
	
	
	
	
}
