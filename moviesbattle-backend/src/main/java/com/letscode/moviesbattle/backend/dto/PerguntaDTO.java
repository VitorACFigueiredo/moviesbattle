package com.letscode.moviesbattle.backend.dto;

import com.letscode.moviesbattle.backend.entity.Pergunta;
import com.letscode.moviesbattle.backend.loading.FilmeDetalheDTO;

public class PerguntaDTO {

	private Long partidaId;
	
	private Long perguntaId;
	
	private Integer filmeComMaiorPontuacao = 0; //1 ou 2

	private FilmeDetalheDTO primeiroFilme;
	
	private FilmeDetalheDTO segundoFilme;

	
	
	public PerguntaDTO() {}
	
	public PerguntaDTO(Pergunta p) {
		this.perguntaId = p.getId();
		this.partidaId = p.getPartida().getId();
		this.primeiroFilme = new FilmeDetalheDTO( p.getParDeFilme().getPrimeiroFilme() );
		this.segundoFilme = new FilmeDetalheDTO( p.getParDeFilme().getSegundoFilme() );
		this.filmeComMaiorPontuacao = p.getFilmeComMaiorPontuacao();
	}

	
	
	
	
	public Long getPartidaId() {
		return partidaId;
	}

	public void setPartidaId(Long partidaId) {
		this.partidaId = partidaId;
	}

	public Long getPerguntaId() {
		return perguntaId;
	}

	public void setPerguntaId(Long perguntaId) {
		this.perguntaId = perguntaId;
	}

	public FilmeDetalheDTO getPrimeiroFilme() {
		return primeiroFilme;
	}

	public void setPrimeiroFilme(FilmeDetalheDTO primeiroFilme) {
		this.primeiroFilme = primeiroFilme;
	}

	public FilmeDetalheDTO getSegundoFilme() {
		return segundoFilme;
	}

	public void setSegundoFilme(FilmeDetalheDTO segundoFilme) {
		this.segundoFilme = segundoFilme;
	}

	public Integer getFilmeComMaiorPontuacao() {
		return filmeComMaiorPontuacao;
	}
	
	public void setFilmeComMaiorPontuacao(Integer filmeComMaiorPontuacao) {
		this.filmeComMaiorPontuacao = filmeComMaiorPontuacao;
	}
	

}
