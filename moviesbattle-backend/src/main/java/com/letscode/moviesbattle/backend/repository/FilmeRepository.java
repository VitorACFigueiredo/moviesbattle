package com.letscode.moviesbattle.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.letscode.moviesbattle.backend.entity.Filme;

@Repository
public interface FilmeRepository extends JpaRepository<Filme, Long> {

	
	@Query("select min(f.id) from Filme f")
	public Long getMinFilmeId();

	
	@Query("select max(f.id) from Filme f")
	public Long getMaxFilmeId();

	

	
}
