package com.letscode.moviesbattle.backend.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Filme implements Serializable {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String title;
	
	@Column(nullable = false)
	private String year;
	
	@Column(nullable = false)
	private String director;
	
	@Column(nullable = false)
	private String actors;
	
	@Column(nullable = false)
	private String country;
	
	@Column(nullable = false)
	private String poster;
	
	
	@Column(nullable = false)
	private String imdbRating; //"imdbRating": "7.4",
	
	@Column(nullable = false)
	private String imdbVotes; //"imdbVotes": "473,820",
	
	@Column(nullable = false)
	private String imdbID; //imdbID
	
	
	private BigDecimal pontuacaoFilme;
	

	public void atualizarPontuacao() throws ParseException {
		//remove eventuais pontos e virgulas:
		DecimalFormat decFmt = new DecimalFormat();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		decFmt.setDecimalFormatSymbols(symbols);
		
		//atualiza Pontuacao com rating e votos
		Number rating = decFmt.parse(this.imdbRating);
		Number votes = decFmt.parse(this.imdbVotes);
		if (rating == null || votes == null) {
			this.pontuacaoFilme = BigDecimal.ZERO;
		}
		this.pontuacaoFilme = new BigDecimal( rating.doubleValue() * votes.doubleValue() );
		this.pontuacaoFilme.round( new MathContext(1) );
	}

	

	
	private static final long serialVersionUID = 2901952325156228999L;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPontuacaoFilme() {
		return pontuacaoFilme;
	}
	public void setPontuacaoFilme(BigDecimal pontuacaoFilme) {
		this.pontuacaoFilme = pontuacaoFilme;
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getImdbVotes() {
		return imdbVotes;
	}

	public void setImdbVotes(String imdbVotes) {
		this.imdbVotes = imdbVotes;
	}

	public String getImdbID() {
		return imdbID;
	}

	public void setImdbID(String imdbID) {
		this.imdbID = imdbID;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Filme other = (Filme) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Filme [id=" + id + ", title=" + title + ", year=" + year + ", imdbID=" + imdbID + ", pontuacaoFilme="
				+ pontuacaoFilme + "]";
	}


	
}
