package com.letscode.moviesbattle.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.letscode.moviesbattle.backend.entity.Filme;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;

/**
 * API para consultar todos os filmes cadastrados na base
 * Nota: não tem funcionalidade própria, apenas para consulta 
 * 
 * @author Vitor.Campos
 * @since 22 jan 2022
 */
@RestController
@RequestMapping("/api/filme")
public class FilmeController {

	@Autowired
	private FilmeRepository repository;

	@GetMapping
	public List<Filme> getFilme() {
		List<Filme> listaFilme = repository.findAll();
		
		return listaFilme;
	}




}
