package com.letscode.moviesbattle.backend.loading;

import java.io.File;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.letscode.moviesbattle.backend.entity.Filme;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;

public class FilmeJsonLoader {
	
	private String path = "src/main/resources/data/";
	private String filename = "filmes.json";

	private FilmeRepository filmeRepo;
	
	private Boolean flagContinue = false;
	
	
	private List<Filme> listaFilme;
	
	
	public FilmeJsonLoader(FilmeRepository filmeRepo) {
		this.filmeRepo = filmeRepo;
	}

	public static FilmeJsonLoader build(FilmeRepository filmeRepo) {
		throwIfNull(filmeRepo);
		return new FilmeJsonLoader(filmeRepo);
	}
	
	
	private static void throwIfNull(FilmeRepository filmeRepo) {
		if (filmeRepo == null) {
			throw new RuntimeException("Instancia FilmeRepository null");
		}
	}
	
	
	public FilmeJsonLoader continueIfDatabaseEmpty() {
		long count = filmeRepo.count();
		if (count > 0) {
			this.flagContinue = false;
		} else {
			this.flagContinue = true;
		}
		return this;
	}

	
	public FilmeJsonLoader readJSON() {
		if (flagContinue) {
			
			try {
				File sourceFile = new File(this.path, this.filename);
				
				ObjectMapper mapper = new ObjectMapper();
				this.listaFilme = mapper.readValue(sourceFile, new TypeReference<List<Filme>>(){} );
				
			} catch (Exception e) {
				throw new RuntimeException( e );
			}
		}
		
		return this;
	}
	
	
	public void writeDatabase() {
		if (flagContinue) {
			
			for (Filme filme : listaFilme) {
				filmeRepo.save(filme);
			}
		}
	}
	
	

	
//	public static void main(String[] args) throws Exception {
//		
//		File sourceFile = new File("src/main/resources/data/", "filmes.json");
//		System.out.println( sourceFile.getAbsolutePath() );
//		System.out.println( sourceFile.getCanonicalPath() );
//		
//		ObjectMapper mapper = new ObjectMapper();
//		List<Filme> listaFilmes = mapper.readValue(sourceFile, new TypeReference<List<Filme>>(){} );
//		
//		System.out.println( listaFilmes );
//	}

}
