package com.letscode.moviesbattle.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.letscode.moviesbattle.backend.dto.PartidaAtivaDTO;
import com.letscode.moviesbattle.backend.dto.PartidaEncerradaDTO;
import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.entity.Pergunta;
import com.letscode.moviesbattle.backend.service.PartidaService;
import com.letscode.moviesbattle.backend.service.PerguntaService;

/**
 * API para gerenciar todas as interaçoes de partidas
 * 
 * @author Vitor.Campos
 * @since 23 jan 2022
 */
@RestController
@RequestMapping("/api/partida")
public class PartidaController {
	
	@Autowired
	private PartidaService partidaService;
	
	@Autowired
	private PerguntaService perguntaService;
	
	
	/**
	 * Inicia nova partida e ja devolve primeira pergunta
	 * @param jogadorDTO
	 * @return primeira pergunta
	 */
	@PostMapping("/{jogadorId}")
	public PartidaAtivaDTO iniciarPartida(@PathVariable("jogadorId") Long jogadorId) {
		Partida novaPartida = partidaService.criarPartida( jogadorId );
		
		Pergunta primeiraPergunta = perguntaService.criarPergunta(novaPartida);
		
		PartidaAtivaDTO partidaDTO = new PartidaAtivaDTO(novaPartida, primeiraPergunta);
		return partidaDTO;
	}
	
	
	/**
	 * Encerra partida por iniciativa do jogador
	 * @param partidaId
	 */
	@DeleteMapping("/{partidaId}")
	public PartidaEncerradaDTO encerrarPartida(@PathVariable("partidaId") Long partidaId) {
		Partida partida = partidaService.encerrarPartida(partidaId);
		
		PartidaEncerradaDTO partidaEncerrada = new PartidaEncerradaDTO(partida);
		return partidaEncerrada;
	}

	
	
	/**
	 * Responde pergunta:
	 * Se certa, devolve com a proxima
	 * Se errada:
	 *    Se menos de 3 erros, devolve o erro e proxima
	 *    Se mais de 3 erros, encerrar a partida
	 * @param perguntaId
	 * @param opcaoRespondida 1=primeiro filme; 2=segundo filme
	 * @return partida ativa com a proxima pergunta
	 */
	@PostMapping("/pergunta/{perguntaId}/opcaoRespondida/{opcaoRespondida}")
	public PartidaAtivaDTO responderPergunta(@PathVariable("perguntaId") Long perguntaId, @PathVariable("opcaoRespondida") int opcaoRespondida) {
		//1.avalia a resposta
		if (perguntaService.foiRespondidaCorretamente(perguntaId, opcaoRespondida)) {
			return registrarPerguntaComoCerta( perguntaId );
		} else {
			return registrarPerguntaComoErrada( perguntaId );
		}
	}



	/**
	 * Registra pergunta como certo salvando-a e retorna partida com a 
	 * proxima pergunta
	 * @param perguntaDTO
	 * @return partida ativa com a proxima pergunta
	 */
	private PartidaAtivaDTO registrarPerguntaComoCerta(Long partidaId) {
		Pergunta perguntaRecemRespondida = perguntaService.salvarPerguntaComoCorreta( partidaId );
		
		PartidaAtivaDTO partidaDTO = getPartidaComProximaPergunta(partidaId, perguntaRecemRespondida);
		return partidaDTO;
	}

	
	/**
	 * Registra pergunta como errada, salvando-a, verificando se limite de 
	 * erros ja foi atingido e retornando partida com proxima perguntda 
	 * @param perguntaDTO
	 * @return partida ativa a proxima pergunta
	 */
	private PartidaAtivaDTO registrarPerguntaComoErrada(Long partidaId) {
		//1.salva pergunda com errada
		Pergunta perguntaRecemRespondida = perguntaService.salvarPerguntaComoErrada( partidaId );
		
		//2.verifica se atingiu o limite de erros para encerrar 
		Partida partida = partidaService.encerrarPartidaSemAtingirLimiteDeErros(partidaId);
		
		//3.se partida ainda ativa, prepara proxima pergunta
		if (! partida.getFlagAtingiuLimiteErro() ) {
			return getPartidaComProximaPergunta(partidaId, perguntaRecemRespondida);
		} else {
			return new PartidaAtivaDTO( partida );
		}
	}
	
	
	
	/**
	 * Recarrega partida atual, cria proxima pergunta e retorna partida com
	 * @param partidaId
	 * @param perguntaRecemRespondida
	 * @return partida ativa com a proxima pergunta
	 */
	private PartidaAtivaDTO getPartidaComProximaPergunta(Long partidaId, Pergunta perguntaRecemRespondida) {
		Partida partida = partidaService.recarregarPartidaPeloId( partidaId );
		
		Pergunta proximaPergunta = perguntaService.criarPergunta(partida);
		
		PartidaAtivaDTO partidaDTO = new PartidaAtivaDTO(partida, perguntaRecemRespondida, proximaPergunta);
		
		return partidaDTO;
	}
	
	
	
	
	
	 

}
