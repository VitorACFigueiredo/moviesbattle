package com.letscode.moviesbattle.backend;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.letscode.moviesbattle.backend.entity.Jogador;
import com.letscode.moviesbattle.backend.loading.FilmeJsonLoader;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;
import com.letscode.moviesbattle.backend.repository.JogadorRepository;
import com.letscode.moviesbattle.backend.repository.UsuarioRepository;
import com.letscode.moviesbattle.backend.security.Usuario;

@SpringBootApplication
public class MoviesbattleBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviesbattleBackendApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner initDB(JogadorRepository jogadorRepo, UsuarioRepository usuarioRepo, FilmeRepository filmeRepo) {
		return args -> {
			//1.jogadores e usuarios
			Jogador j1 = new Jogador();
			j1.setNome("João Pedro");
			j1 = jogadorRepo.save(j1);
			
			Usuario u1 = new Usuario();
			u1.setJogador(j1);
			u1.setLogin("joao");
			u1.setSenha( new BCryptPasswordEncoder().encode("123") );
			u1 = usuarioRepo.save(u1);
			
			
			Jogador j2 = new Jogador();
			j2.setNome("Julia Botrel");
			j2 = jogadorRepo.save(j2);
			
			Usuario u2 = new Usuario();
			u2.setJogador(j2);
			u2.setLogin("julia");
			u2.setSenha(new BCryptPasswordEncoder().encode("123"));
			u2 = usuarioRepo.save(u2);
			
			//2.filmes
			FilmeJsonLoader.build(filmeRepo)
				.continueIfDatabaseEmpty()
				.readJSON()
				.writeDatabase();
		};
	}

}
