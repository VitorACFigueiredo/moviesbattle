package com.letscode.moviesbattle.backend.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.letscode.moviesbattle.backend.enums.PontuacaoPergunta;

@Entity
public class Pergunta implements Serializable {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	private Partida partida;
	
	@ManyToOne
	private ParDeFilme parDeFilme;
	
	/**
	 * Resposta correta vale 1 ponto
	 */
	private Integer pontuacaoPergunta = -1;
	
	/**
	 * Guarda qual é a resposta correta
	 */
	private Integer filmeComMaiorPontuacao = 0;


	
	public Pergunta() {
	}
	
	public Pergunta(Partida partida, ParDeFilme parDeFilme) {
		this.partida = partida;
		this.parDeFilme = parDeFilme;
		this.filmeComMaiorPontuacao = parDeFilme.avaliarFilmeComMaiorPontuacao();
	}
	
	
	public Boolean foiRespondidaCorretamente(int opcaoRespondida) {
		if (this.getFilmeComMaiorPontuacao() == opcaoRespondida ) {
			return true;
			
		} else {
			return false;
		}
	}
	
	
	public void registrarAcerto() {
		this.pontuacaoPergunta = PontuacaoPergunta.RESPOSTA_CORRETA.pontos;
	}
	
	
	public void registarComoIncorreto() {
		this.pontuacaoPergunta = PontuacaoPergunta.RESPOSTA_ERRADA.pontos;
	}
	
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8002200444867447568L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFilmeComMaiorPontuacao() {
		return filmeComMaiorPontuacao;
	}

	public void setFilmeComMaiorPontuacao(Integer filmeComMaiorPontuacao) {
		this.filmeComMaiorPontuacao = filmeComMaiorPontuacao;
	}

	public Partida getPartida() {
		return partida;
	}

	public void setPartida(Partida partida) {
		this.partida = partida;
	}

	public ParDeFilme getParDeFilme() {
		return parDeFilme;
	}

	public void setParDeFilme(ParDeFilme parDeFilme) {
		this.parDeFilme = parDeFilme;
	}

	public Integer getPontuacaoPergunta() {
		return pontuacaoPergunta;
	}

	public void setPontuacaoPergunta(Integer pontuacaoPergunta) {
		this.pontuacaoPergunta = pontuacaoPergunta;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pergunta other = (Pergunta) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Pergunta [id=" + id + ", pontuacaoPergunta=" + pontuacaoPergunta
				+ ", filmeComMaiorPontuacao=" + filmeComMaiorPontuacao + "]";
	}

	
}
