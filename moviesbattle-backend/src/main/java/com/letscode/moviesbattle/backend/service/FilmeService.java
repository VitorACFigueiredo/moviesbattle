package com.letscode.moviesbattle.backend.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.letscode.moviesbattle.backend.entity.Filme;
import com.letscode.moviesbattle.backend.entity.ParDeFilme;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;
import com.letscode.moviesbattle.backend.repository.ParDeFilmeRepository;

/**
 * Serviço para filme e par de filmes
 * 
 * @author Vitor.Campos
 * @since 22 jan 2022
 */
@Service
public class FilmeService {
	
	@Autowired
	private FilmeRepository filmeRepository;
	
	@Autowired
	private ParDeFilmeRepository parDeFilmeRepository;
	

	/**
	 * Cria um par de filme único usando IDs 
	 * randômicos
	 * @return par de filme
	 */
	public ParDeFilme criarParDeFilmeUnico() {
		ParDeFilme parUnico = instanciarParUnico();
		parUnico = parDeFilmeRepository.save(parUnico);
		return parUnico;
	}

	
	
	/**
	 * Recupera o ID minimo e maximo da PK de filme
	 * e retorna um valor intermediário aleatório
	 * @return
	 */
	private Long gerarRandomFilmeId() {
		Long minID = filmeRepository.getMinFilmeId();
		Long maxID = filmeRepository.getMaxFilmeId();
		
		long idRandom = minID + (long) (Math.random() * (maxID - minID));
		return idRandom;
	}


	/**
	 * Busca 2 filmes que são pares únicos
	 * @return
	 */
	private ParDeFilme instanciarParUnico() {
		while(true) {
			Filme primeiroFilme = buscarPrimeiroFilme();
			
			Filme segundoFilme = buscarSegundoFilme(primeiroFilme);
			
			String imcbIdCombinado = ParDeFilme.Util.combinarImcbId(primeiroFilme, segundoFilme);
			ParDeFilme parEncontrado = parDeFilmeRepository.findByImdbIdCombinado(imcbIdCombinado);
			if (parEncontrado == null) {//se nada foi encontrado, o par é unico
				ParDeFilme parUnico = new ParDeFilme(primeiroFilme, segundoFilme);
				return parUnico;
			}
		}
	}
	
	
	/**
	 * Busca um primeiro filme usando PK aleatória
	 * @return
	 */
	private Filme buscarPrimeiroFilme() {
		while(true) {
			long randId = gerarRandomFilmeId();
			Optional<Filme> opFilme = filmeRepository.findById(randId);
			if (opFilme.isPresent()) {
				return opFilme.get();
			}
		}
	}
	
	/**
	 * Busca o segundo filme usando PK aleatória
	 * e que seja diferente do primeiro filme
	 * @param primeiroFilme
	 * @return
	 */
	private Filme buscarSegundoFilme(Filme primeiroFilme) {
		while(true) {
			Long randId = gerarRandomFilmeId();
			Optional<Filme> opFilme = filmeRepository.findById(randId);
			if (opFilme.isPresent()) {
				Filme segundoFilme = opFilme.get();
				if (! segundoFilme.equals(primeiroFilme)) {
					return segundoFilme;
				}
			}
		}
	}
	

}
