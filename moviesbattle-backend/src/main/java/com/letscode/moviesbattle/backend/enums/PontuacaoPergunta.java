package com.letscode.moviesbattle.backend.enums;

public enum PontuacaoPergunta {
	
	RESPOSTA_ERRADA(0)
	,
	RESPOSTA_CORRETA(1)
	;
	
	public final Integer pontos;
	
	private PontuacaoPergunta(Integer p) {
		this.pontos = p;
	}

}
