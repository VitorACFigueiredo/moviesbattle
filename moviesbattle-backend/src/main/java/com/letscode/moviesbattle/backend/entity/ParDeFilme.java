package com.letscode.moviesbattle.backend.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.letscode.moviesbattle.backend.enums.OpcaoDeResposta;

@Entity
public class ParDeFilme implements Serializable {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	private Filme primeiroFilme;
	
	@ManyToOne
	private Filme segundoFilme;
	
	/**
	 * Combinado dos imdbID dos 2 filmes 
	 */
	@Column(nullable =  false)
	private String imdbIdCombinado;
	
	
	
	//construtores...
	
	public ParDeFilme() {
		
	}
	
	public ParDeFilme(Filme primeiroFilme, Filme segundoFilme) {
		super();
		this.primeiroFilme = primeiroFilme;
		this.segundoFilme = segundoFilme;
		this.imdbIdCombinado = Util.combinarImcbId(primeiroFilme, segundoFilme);
	}


	/**
	 * Avalia qual é o filme com maior pontuacao
	 * @return 1 ou 2
	 */
	public Integer avaliarFilmeComMaiorPontuacao() {
		if (this.primeiroFilme.getPontuacaoFilme().doubleValue() > this.segundoFilme.getPontuacaoFilme().doubleValue()) {
			return OpcaoDeResposta.PRIMEIRO_FILME.indice;
		} else {
			return OpcaoDeResposta.SEGUNDO_FILME.indice;
		}
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 2881259980823053622L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Filme getPrimeiroFilme() {
		return primeiroFilme;
	}

	public void setPrimeiroFilme(Filme primeiroFilme) {
		this.primeiroFilme = primeiroFilme;
	}

	public Filme getSegundoFilme() {
		return segundoFilme;
	}

	public void setSegundoFilme(Filme segundoFilme) {
		this.segundoFilme = segundoFilme;
	}

	public String getImdbIdCombinado() {
		return imdbIdCombinado;
	}

	public void setImdbIdCombinado(String imdbIdCombinado) {
		this.imdbIdCombinado = imdbIdCombinado;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParDeFilme other = (ParDeFilme) obj;
		return Objects.equals(id, other.id);
	}
	
	@Override
	public String toString() {
		return "ParDeFilme [id=" + id + ", imdbIdCombinado=" + imdbIdCombinado + "]";
	}



	public static class Util {
		private static final String TEMPLATE_IMDBID_COMBINADO = "%s-%s"; 

		/**
		 * Combinar os imdbID dos 2 filmes 
		 * concatenando-se em ordem alfabética 
		 */
		public static String combinarImcbId(Filme primeiroFilme, Filme segundoFilme) {
			String imdbID_1 = primeiroFilme.getImdbID(); 
			String imdbID_2 = segundoFilme.getImdbID();
			if (imdbID_1.compareTo(imdbID_2) < 0) {//ordem alfabetica
				return String.format(TEMPLATE_IMDBID_COMBINADO, imdbID_1, imdbID_2);
			} else {
				return String.format(TEMPLATE_IMDBID_COMBINADO, imdbID_2, imdbID_1);
			}
		}
	}

	
}
