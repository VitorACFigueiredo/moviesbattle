package com.letscode.moviesbattle.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.letscode.moviesbattle.backend.entity.ParDeFilme;

@Repository
public interface ParDeFilmeRepository extends JpaRepository<ParDeFilme, Long> {
	
	@Query("select p from ParDeFilme p where p.imdbIdCombinado = :pImdbIdCombinado")
	public ParDeFilme findByImdbIdCombinado(@Param("pImdbIdCombinado") String imcbIdCombinado);
	

}
