package com.letscode.moviesbattle.backend.dto;

import java.math.BigDecimal;
import java.math.MathContext;


public class RankingDTO implements Comparable<RankingDTO> {
	
	private int colocacao;
	
	private Long jogadorId;
	
	private String jogadorNome;
	
	private Long quantidadePartidasJogadas = -1L;
	
	private Long quantidadeRespostasCorretas = -1L;

	private Long quantidadePerguntas = -1L;
	
	
	private BigDecimal pontuacaoFinal = BigDecimal.ZERO;



	public RankingDTO( Long jogadorId 
					 , String jogadorNome
					 , Long qtdPartidas
					 , Long qtdRespostasCorretas
					 , Long qtdPerguntas
					 ) {
		this.jogadorId = jogadorId;
		this.jogadorNome = jogadorNome;
		this.quantidadePartidasJogadas = qtdPartidas;
		this.quantidadeRespostasCorretas = qtdRespostasCorretas;
		this.quantidadePerguntas = qtdPerguntas;
		
		this.calcularPontuacaoFinal();
	}


	/**
	 *  "A pontuação é obtida multiplicando a quantidade de quizzes (partida) 
	 *  respondidas pelo porcentual de acerto."
	 */
	private void calcularPontuacaoFinal() {
		double percentualAcerto = 0.0;
		if (this.quantidadePerguntas != 0) {
			percentualAcerto = (double)this.quantidadeRespostasCorretas / this.quantidadePerguntas;
		}
		
		this.pontuacaoFinal = new BigDecimal( this.quantidadePartidasJogadas * percentualAcerto );
		this.pontuacaoFinal.round( new MathContext(1) );
	}

	

	public int getColocacao() {
		return colocacao;
	}

	public Long getQuantidadePartidasJogadas() {
		return quantidadePartidasJogadas;
	}

	public void setQuantidadePartidasJogadas(Long quantidadePartidasJogadas) {
		this.quantidadePartidasJogadas = quantidadePartidasJogadas;
	}

	public Long getJogadorId() {
		return jogadorId;
	}

	public String getJogadorNome() {
		return jogadorNome;
	}

	public Long getQuantidadeRespostasCorretas() {
		return quantidadeRespostasCorretas;
	}

	public Long getQuantidadePerguntas() {
		return quantidadePerguntas;
	}

	public BigDecimal getPontuacaoFinal() {
		return pontuacaoFinal;
	}

	public void setColocacao(int colocacao) {
		this.colocacao = colocacao;
	}

	@Override
	public String toString() {
		return "RankingDTO [colocacao=" + colocacao + ", jogadorNome=" + jogadorNome + ", quantidadePartidasJogadas="
				+ quantidadePartidasJogadas + ", quantidadeRespostasCorretas=" + quantidadeRespostasCorretas
				+ ", quantidadePerguntas=" + quantidadePerguntas + ", pontuacaoFinal=" + pontuacaoFinal + "]";
	}

	/**
	 * Ordem crescente de pontuacaoFinal
	 */
	@Override
	public int compareTo(RankingDTO that) {
		return this.pontuacaoFinal.compareTo(that.pontuacaoFinal);
	}
}
