package com.letscode.moviesbattle.backend.loading;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

public class SearchResultDTO {
	
	@JsonAlias("Search")
	private FilmeSumarioDTO[] search;

	
	public FilmeSumarioDTO[] getSearch() {
		return search;
	}

	public void setSearch(FilmeSumarioDTO[] search) {
		this.search = search;
	}

	public List<FilmeSumarioDTO> toListFilmeSumario() {
		return Arrays.asList( search );
	}
	

	
}
