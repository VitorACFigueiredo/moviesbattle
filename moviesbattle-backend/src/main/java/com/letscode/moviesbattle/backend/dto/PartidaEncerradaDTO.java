package com.letscode.moviesbattle.backend.dto;

import com.letscode.moviesbattle.backend.entity.Partida;

public class PartidaEncerradaDTO {

	private Long partidaId;
		
	private Integer quantidadeRespostasCorretas = -1;

	private Integer quantidadePerguntas = -1;
	

	private Boolean flagAtingiuLimiteErro = false;
	
	private Boolean flagEncerrada = false;

	
	public PartidaEncerradaDTO(Partida p) {
		this.partidaId = p.getId();
		this.quantidadeRespostasCorretas = p.getQuantidadeRespostasCorretas();
		this.quantidadePerguntas = p.getQuantidadePerguntas();
		this.flagAtingiuLimiteErro = p.getFlagAtingiuLimiteErro();
		this.flagEncerrada = p.getFlagEncerrada();
	}


	
	public Long getPartidaId() {
		return partidaId;
	}


	public Integer getQuantidadeRespostasCorretas() {
		return quantidadeRespostasCorretas;
	}


	public Integer getQuantidadePerguntas() {
		return quantidadePerguntas;
	}


	public Boolean getFlagAtingiuLimiteErro() {
		return flagAtingiuLimiteErro;
	}


	public Boolean getFlagEncerrada() {
		return flagEncerrada;
	}
}