package com.letscode.moviesbattle.backend.service;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.letscode.moviesbattle.backend.entity.Filme;
import com.letscode.moviesbattle.backend.entity.ParDeFilme;
import com.letscode.moviesbattle.backend.loading.FilmeJsonLoader;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;

@SpringBootTest
class FilmeServiceTest {

	@Autowired
	private FilmeService filmeService;
	
	@Autowired
	private FilmeRepository filmeRepo;
	
	
	@BeforeEach
	void setup() {//carrega base com filmes
		FilmeJsonLoader.build(filmeRepo)
			.continueIfDatabaseEmpty()
			.readJSON()
			.writeDatabase();
	}
	
	
	@Test
	public void quandoCriarParDeFilmeUnico_entaoParDeFilmeEhUnico() {
		ParDeFilme parDeFilme = filmeService.criarParDeFilmeUnico();
		
		Filme f1 = parDeFilme.getPrimeiroFilme();
		Filme f2 = parDeFilme.getSegundoFilme();
		assertNotEquals( f1.getImdbID(), f2.getImdbID() );
	}
	
	
	

}
