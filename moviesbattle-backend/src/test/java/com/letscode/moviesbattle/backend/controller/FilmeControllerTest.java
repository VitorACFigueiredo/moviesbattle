package com.letscode.moviesbattle.backend.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class FilmeControllerTest {

	private static final String BASE_URL = "/api/filme";
	
	@Autowired
	private MockMvc mock;
	
	@Test
	public void quandoGetApiFilme_entaoStatusOK() throws Exception {
		
		mock.perform(
				get(BASE_URL)
					.contentType( MediaType.APPLICATION_JSON )
				)
				.andExpect( status().isOk() )
				;
	}
	
	/* exemplo de analisando o conteudo da resposta...
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello, World")));
	}	 * 
	 */
	

}
