package com.letscode.moviesbattle.backend.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.letscode.moviesbattle.backend.security.LoginForm;

@SpringBootTest
@AutoConfigureMockMvc
public class AutenticacaoControllerTest {
	
	private static final String BASE_URL = "/auth";
	
	@Autowired
	private MockMvc mock;
	
	@Test
	public void dadoLoginAndSenhaCorreto_quandoAutenticar_entaoStatusOK() throws Exception {
		LoginForm loginForm = new LoginForm("joao", "123");
		
		String asJson = new ObjectMapper().writeValueAsString(loginForm);
		
		mock.perform(
				post(BASE_URL)
			      	.content( asJson )
					.contentType( MediaType.APPLICATION_JSON )
				)
				.andExpect( status().isOk() )
				;
	}


	@Test
	public void dadoLoginAndSenhaErrados_quandoAutenticar_entaoStatusNaoAutorizado() throws Exception {
		LoginForm loginForm = new LoginForm("LoginErrado", "SenhaErrada");
		
		String asJson = new ObjectMapper().writeValueAsString(loginForm);
		
		mock.perform(
				post(BASE_URL)
			      	.content( asJson )
					.contentType( MediaType.APPLICATION_JSON )
				)
				.andExpect( status().isUnauthorized() )
				;
	}

	

}
