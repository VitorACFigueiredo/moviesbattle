package com.letscode.moviesbattle.backend.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.entity.Pergunta;
import com.letscode.moviesbattle.backend.enums.OpcaoDeResposta;
import com.letscode.moviesbattle.backend.loading.FilmeJsonLoader;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;
import com.letscode.moviesbattle.backend.repository.JogadorRepository;

@SpringBootTest
class PerguntaServiceTest {

	@Autowired
	private PerguntaService perguntaService;
	
	
	@Autowired
	private FilmeRepository filmeRepo;

	@Autowired
	private JogadorRepository jogadorRepo;

	@Autowired
	private PartidaService partidaService;
	
	
	
	@BeforeEach
	void setUpBeforeClass() throws Exception {
		FilmeJsonLoader.build( filmeRepo )
			.continueIfDatabaseEmpty()
			.readJSON()
			.writeDatabase();
	}

	private Pergunta criarPergunta(Partida partida) {
		return perguntaService.criarPergunta( partida );
	}

	
	
	@Test
	void dadoPartidaValida_quandoCriarPergunta_entaoPergundaCriadaEhValida() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		
		Pergunta pergunta = criarPergunta(partida);
		assertNotNull( pergunta );
		assertNotNull( pergunta.getId() );
		
		
	}



	
	@Test
	void dadoOpcaoRespondida_quandoVerificarSeFoiRespondidaCorretamente_entaoResultadoEhCoerente() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		
		Pergunta p = criarPergunta(partida); 
	
		//1.respondido certo
		
		Integer opcaoRespondida = p.getFilmeComMaiorPontuacao();
		
		Boolean resultado = perguntaService.foiRespondidaCorretamente(p.getId(), opcaoRespondida);
		assertTrue( resultado );
	
		
		//2.respondido errado
		
		if (opcaoRespondida == OpcaoDeResposta.PRIMEIRO_FILME.indice) {//trocar as respostas
			opcaoRespondida = OpcaoDeResposta.SEGUNDO_FILME.indice;
		} else {
			opcaoRespondida = OpcaoDeResposta.PRIMEIRO_FILME.indice;
		}
		
		resultado = perguntaService.foiRespondidaCorretamente(p.getId(), opcaoRespondida);
		assertFalse( resultado );
	}
	
	

	
	
	
	@Test
	void dadoPerguntaErrada_quandoSalvarPerguntaComoErrada_entaoPontuacaoEhZero() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		Pergunta perguntaErrada = criarPergunta(partida);
		
		Pergunta perguntaResultado = perguntaService.salvarPerguntaComoErrada( perguntaErrada.getId() );

		//1.pontuacao dever ser zero
		assertTrue( perguntaResultado.getPontuacaoPergunta() == 0 );
	}

	
	@Test
	void dadoPerguntaErrada_quandoSalvarPerguntaComoErrada_entaoContadorDeErrosDaPartidaFoiIncrementado() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		Pergunta perguntaErrada = criarPergunta(partida);
		
		Integer contadorInicial = partida.getContadorPerguntasErradas();
		
		Pergunta perguntaResultado = perguntaService.salvarPerguntaComoErrada( perguntaErrada.getId() );

		partida = partidaService.recarregarPartidaPeloId( partida.getId() );
		Integer contadorFinal = partida.getContadorPerguntasErradas();
		
		assertTrue( contadorFinal > contadorInicial );
	}
	
	

	
	
	@Test
	void dadoPerguntaCerta_quandoSalvarPerguntaComoCorreta_entaoPontuacaoEhUm() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		Pergunta pCorreta = criarPergunta(partida);
		
		Pergunta perguntaResultado = perguntaService.salvarPerguntaComoCorreta( pCorreta.getId() );
		assertTrue( perguntaResultado.getPontuacaoPergunta() == 1 );
	}
	
	
}
