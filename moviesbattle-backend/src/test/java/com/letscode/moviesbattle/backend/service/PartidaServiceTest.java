package com.letscode.moviesbattle.backend.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.repository.JogadorRepository;
import com.letscode.moviesbattle.backend.repository.PartidaRepository;

@SpringBootTest
class PartidaServiceTest {

	@Autowired
	private PartidaService partidaService;
	
	@Autowired
	private JogadorRepository jogadorRepo;
	
	@Autowired
	private PartidaRepository partidaRepo;
	
	
	
	
	// criarPartida
	
	@Test
	Partida dadoJogadorValido_quandoCriarPartida_entaoPartidaEhCriada() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		assertNotNull( partida );
		assertNotNull( partida.getId() );
		
		return partida;
	}
	

	// encerrarPartida
	
	@Test
	void dadoIdPartidaValido_quandoEncerrarPartida_entaoFlagSetadosCorretamente() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		Long idPartidaValido = partida.getId();
		
		Partida partidaEncerrada = partidaService.encerrarPartida( idPartidaValido );
		assertTrue( partidaEncerrada.getFlagEncerrada() );
		assertFalse( partidaEncerrada.getFlagAtingiuLimiteErro() );
	}
	

	
	// encerrarPartidaSeAtingirLimiteDeErros

	@Test
	void dadoIdPartidaValidoCom3Erros_quandoEncerrarPartidaSemAtingirLimiteDeErros_entaoFlagsEncerradosTrue() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		Long idPartidaValido = partida.getId();
		
		partida.incrementarContadorPerguntasErradas();
		partida.incrementarContadorPerguntasErradas();
		partida.incrementarContadorPerguntasErradas();//3 erros: atingiu limite
		partida = partidaRepo.save(partida);
		
		Partida partidaEncerrada = partidaService.encerrarPartidaSemAtingirLimiteDeErros(idPartidaValido);
		assertTrue( partidaEncerrada.getFlagEncerrada() );
		assertTrue( partidaEncerrada.getFlagAtingiuLimiteErro() );
	}

	@Test
	void dadoIdPartidaValidoCom2ErrosApenas_quandoEncerrarPartidaSemAtingirLimiteDeErros_entaoFlagEncerradoFalse() {
		Partida partida = TestHelper.criarPartida(jogadorRepo, partidaService);
		Long idPartidaValido = partida.getId();
		
		partida.incrementarContadorPerguntasErradas();
		partida.incrementarContadorPerguntasErradas();//2 erros apenas
		partida = partidaRepo.save(partida);
		
		Partida partidaEncerrada = partidaService.encerrarPartidaSemAtingirLimiteDeErros(idPartidaValido);
		assertFalse( partidaEncerrada.getFlagEncerrada() );
		assertFalse( partidaEncerrada.getFlagAtingiuLimiteErro() );
	}

	
	
	// recarregarPartidaPeloId

	@Test
	void dadoPartidaIdValido_quandoRecarregarPartidaPeloId_entaoPartidaValida() {
		Long idPartidaValida = TestHelper.criarPartida(jogadorRepo, partidaService).getId();
		try {
			Partida partidaValida = partidaService.recarregarPartidaPeloId( idPartidaValida );
			assertNotNull( partidaValida );
			
		} catch (Exception e) {
			fail("Não devia ter lançado exception");
		}
	}
	
	@Test
	void dadoPartidaIdInexistente_quandoRecarregarPartidaPeloId_entaoException() {
		Long idPartidaInexistente = -234324234L;
		try {
			partidaService.recarregarPartidaPeloId( idPartidaInexistente );
			fail("Devia ter lançado exception");
			
		} catch (Exception e) {
			System.out.println("Msg esperada: " + e.getMessage() );
		}
	}
	
	

}
