package com.letscode.moviesbattle.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.letscode.moviesbattle.backend.dto.RankingDTO;
import com.letscode.moviesbattle.backend.entity.Jogador;
import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.entity.Pergunta;
import com.letscode.moviesbattle.backend.loading.FilmeJsonLoader;
import com.letscode.moviesbattle.backend.repository.FilmeRepository;
import com.letscode.moviesbattle.backend.repository.JogadorRepository;

@SpringBootTest
class RankingServiceTest {

	
	@Autowired
	private RankingService rankingService;
	
	@Autowired
	private JogadorRepository jogadorRepo;
	
	@Autowired
	private PartidaService partidaService;
	
	@Autowired
	private PerguntaService perguntaService;

	@Autowired
	private FilmeRepository filmeRepo;
	
	
	@BeforeEach
	void setup() {
		FilmeJsonLoader.build(filmeRepo)
			.continueIfDatabaseEmpty()
			.readJSON()
			.writeDatabase();
	}
	
	
	@Test
	void dadoSimuladoPartidas_quandoConsultarRanking_entaoRankingTem2JogadorEJogadorVencedor() {
		Jogador jogadorJoao = TestHelper.buscarJogador1(jogadorRepo);
		simularPartidaDeJogadoComPeguntasCertasEErradas(jogadorJoao, 3, 1);

		Jogador jogadorJulia = TestHelper.buscarJogador2(jogadorRepo);
		simularPartidaDeJogadoComPeguntasCertasEErradas(jogadorJulia, 4, 0);//sera o vencedor
		simularPartidaDeJogadoComPeguntasCertasEErradas(jogadorJulia, 9, 1);//sera o vencedor
		
		List<RankingDTO> listaRanking = rankingService.pesquisarRanking();
		assertTrue( listaRanking.size() == 2);
		
		RankingDTO rankingVencedor = listaRanking.get(0);//primeiro posicao
		assertEquals( rankingVencedor.getJogadorId(), jogadorJulia.getId() );
		
	}

	
	
	private void simularPartidaDeJogadoComPeguntasCertasEErradas(Jogador jogador1, int qtdCertas, int qtdErradas) {
		Partida partida = partidaService.criarPartida(jogador1.getId());
		
		for (int i = 0; i < qtdCertas; i++) {
			Pergunta pCerta = perguntaService.criarPergunta(partida);
			pCerta = perguntaService.salvarPerguntaComoCorreta( pCerta.getId() );
		}
		
		for (int j=0; j < qtdErradas; j++) {
			Pergunta pErrada = perguntaService.criarPergunta(partida);
			pErrada = perguntaService.salvarPerguntaComoErrada( pErrada.getId() );
		}
		
		partida = partidaService.encerrarPartida( partida.getId() );
	}

}
