package com.letscode.moviesbattle.backend.service;

import com.letscode.moviesbattle.backend.entity.Jogador;
import com.letscode.moviesbattle.backend.entity.Partida;
import com.letscode.moviesbattle.backend.repository.JogadorRepository;

public class TestHelper {
	
	
	protected static Partida criarPartida(JogadorRepository jogadorRepo, PartidaService partidaService) {
		Jogador jogador = buscarJogador1(jogadorRepo);
		Partida partida = partidaService.criarPartida(jogador.getId());
		return partida;
	}

	
	protected static Jogador buscarJogador1(JogadorRepository jogadorRepo) {
		return jogadorRepo.findAll().get(0);
	}
	
	
	protected static Jogador buscarJogador2(JogadorRepository jogadorRepo) {
		return jogadorRepo.findAll().get(1);
	}

	
	
}
