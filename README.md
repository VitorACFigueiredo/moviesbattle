**Jogo Movie Battle**

Esta API gerencia o jogo Movie Battle proposto pela Let´s Code no processo seletivo.

---

## Arquitetura

Foi desenvolvimento em Java 11, com Spring Boot, Spring Security para autenticação (apenas configura

1. Java 11
2. Spring Boot RESt para implementar as API
3. Spring Security para autenticação (apenas configuração sem )
4. Spring Data para persistência no banco de dados
5. Spring Boot Test e JUnit para testes unitários tanto de Services quanto Controller

---

## Carga de dados
Formam criados 2 jogadores/usuários

Foram cadastrados 190 filmes usando o site //www.omdbapi.com. Sempre a aplicação sobe, os filmes são carregados.
Existe um endpoint (/api/filme) apenas para consulta de teste pois não há requisito funcional.